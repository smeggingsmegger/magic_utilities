from django.core.exceptions import ObjectDoesNotExist

from django.core.management import setup_environ
import settings

setup_environ(settings)
from tournament.models import Player, Match

DEFAULT_PLAYERS = [
'Charles',
'Ben',
'Jessie',
'Joel',
'Jake',
'Matt',
'Maranda',
'Amanda',
'Myles',
'Josh',
'Ryan M.',
'Eric',
]
if __name__ == '__main__':
    for player_name in DEFAULT_PLAYERS:
        try:
            player = Player.objects.get(name=player_name)
            print '%s found!' % player_name
        except ObjectDoesNotExist:
            player = Player(name=player_name)
            player.save()
            print '%s Saved!' % player_name

