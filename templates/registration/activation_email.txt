{% load url from future %}

Thank you for registering for our PySGF Magic Card Utility!

Follow this link to activate your profile: http://{{ site.domain }}{% url 'registration.views.activate' activation_key %}

This linkwill expire in {{ expiration_days }} days.
