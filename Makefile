COFFEE = node/node_modules/.bin/coffee
COFFEE_FILES = $(wildcard node/*.coffee mtg/static/js/*.coffee) 
JS_FILES = $(COFFEE_FILES:%.coffee=%.js)

all: nodejs $(JS_FILES)

nodejs:
	@hash node 2>&- || { echo >&2 "Could not locate node. Please install it."; exit 1; }
	@cd node && $(MAKE) 

clean:
	rm -f $(JS_FILES)

%.js: %.coffee
	$(COFFEE) -c $<

start: nodejs $(JS_FILES)
	./start.sh
