function escapeRegExp(my_string) {
    return my_string.replace(/([-.*+?^${}()|[\]\/\\])/g, '\\$1');
}
