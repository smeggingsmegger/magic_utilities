from django.conf.urls.defaults import patterns, include, url
from django.contrib.auth.views import password_reset

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'magic_utilities.views.home', name='home'),
    # url(r'^magic_utilities/', include('magic_utilities.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^tournaments.*$', 'magic_utilities.mtg.tournament.index'),
    url(r'^$', 'magic_utilities.mtg.views.index'),
    (r'^ajax/$', 'magic_utilities.mtg.views.ajax', {}, 'ajax'),
    (r'^deck_manager/$', 'magic_utilities.mtg.deck.deck_manager', {}, 'deck_manager'),
    (r'^favorites/$', 'magic_utilities.mtg.views.favorites', {}, 'favorites'),
    (r'^accounts/', include('registration.backends.default.urls')),
    (r'^accounts/profile/$', 'magic_utilities.mtg.views.profile'),
    (r'^accounts/password/reset/$', password_reset, {'template_name': 'registration/password_reset.html'}),
    (r'^duel/', 'magic_utilities.mtg.views.duel', {}),
    (r'^search/$', 'magic_utilities.mtg.views.search', {}),
    (r'^analyze/$', 'magic_utilities.mtg.views.analyze', {}),
    (r'^analyze/data\.js$', 'magic_utilities.mtg.views.analyze_data_js', {}, 'analyze_data_js'),
    (r'^proxy/$', 'magic_utilities.mtg.views.proxy', {}),
    (r'^advancedsearch/', include('haystack.urls')),
)
