from magic_utilities.mtg.models import Card, Edition, Deck, DeckCard, Favorite
from views import get_user, get_current_deck, JAVASCRIPT_FILES
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse # Http404
from django.template import RequestContext
from django.views.decorators.http import require_http_methods
# from django.views.decorators.csrf import requires_csrf_token, csrf_exempt
from django.shortcuts import get_object_or_404, render, render_to_response

def post_create_deck(request):
    results = {'success': False, 'message': 'You are not logged in.', 'deck': None}
    name = request.POST.get('deck_name', '')
    description = request.POST.get('deck_description', '')
    user = get_user(request)
    if user:
        if name:
            deck = Deck(name=name, description=description, user=user)
            deck.save()
            results['deck'] = {'id': deck.id, 'name': name, 'description': description}
            request.session['current_deck'] = deck
            results['message'] = 'Deck saved!'
            results['success'] = True
        else:
            results['message'] = 'You must name your deck.'
    return results

def select_deck(request):
    deck_id = request.POST.get('deck_id', None)
    result = {'success': False, 'message': 'Not working.'}
    user = get_user(request)
    if not deck_id:
        result['message'] = "Not a valid deck ID."
    elif not user:
        result['message'] = "Not logged in."
    else:
        deck = Deck.objects.filter(user=user).filter(id=deck_id)[0]
        result['success'] = True
        result['message'] = "Deck '%s' selected." % deck.name
        request.session['current_deck'] = deck

    return result

def current_deck(request):
    """
    """
    result = {'success': False, 'message': 'Not working.'}
    user = get_user(request)
    current_deck = get_current_deck(request)
    if not user:
        result['message'] = "Not logged in."
    elif not current_deck:
        result['message'] = "No deck selected."
    else:
        deck_cards = DeckCard.objects.filter(deck=current_deck)
        result['success'] = True
        result['deck_name'] = current_deck.name
        cards = [{'deck_card': deck_card, 'card': deck_card.card} for deck_card in deck_cards]
        result['cards'] = []
        result['message'] = 'Deck delivered...'
        for obj in cards:
            result['cards'].append({
                'id': obj['card'].id,
                'name': obj['card'].name,
                'quantity': obj['deck_card'].quantity,
            })
    return result

def add_card_to_deck(request):
    """
    Adds a card to a deck.
    """
    card_id = request.POST.get('card_id', None)
    quantity = request.POST.get('count', 1)
    result = {'success': False, 'message': 'Not working.'}
    user = get_user(request)
    current_deck = get_current_deck(request)
    if not card_id:
        result['message'] = "Not a valid card ID."
    elif not user:
        result['message'] = "Not logged in."
    elif not current_deck:
        result['message'] = "No deck selected."
    else:
        try:
            card = Card.objects.filter(id=card_id)[0]
        except KeyError:
            card = None
        if card:
            dcs = DeckCard.objects.filter(deck=current_deck, card=card)
            if dcs.count() > 1:
                deck_card = dcs[0]
                deck_card.quantity = sum(dcs.values_list('quantity',
                        flat=True))
                for dc in dcs[1:]:
                    dc.delete()
            elif dcs.count() == 0:
                deck_card = DeckCard(deck=current_deck, card=card, quantity=0)
            else:
                deck_card = dcs[0]

            quantity = int(quantity)
            if quantity + deck_card.quantity <= 4 or u"Land" in card.type:
                deck_card.quantity += quantity
                deck_card.save()
                request.session['current_deck'] = current_deck
                result['success'] = True
                result['message'] = "Card: %s added to deck: %s" % (card.name, current_deck.name)
            else:
                deck_card.save()
                request.session['current_deck'] = current_deck
                result['message'] = "You cannot have more than 4 of a non-land card in your deck."
        else:
            result['message'] = "Not a valid card ID."
    return result

def deck_manager(request):
    user = get_user(request)
    if not user:
        return HttpResponseRedirect("/accounts/login/")
#     current_deck = get_current_deck(request)
    cards = Card.objects.filter(name__istartswith='llanowar elves').all()
    decks = Deck.objects.filter(user=user).all()
    return render(request, 'deck_manager.html', {'cards': cards, 'decks': decks, 'js_files': JAVASCRIPT_FILES})

def post_search_cards(request):
    results = {'success': False, 'message': 'You are not logged in.', 'cards': []}
    search_string = request.POST.get('search_string', None)
    user = get_user(request)
    if user:
        cards = Card.objects.filter(name__icontains=search_string).all()
        for card in cards:
            results['cards'].append({
                'id': card.id,
                'image_url': card.image_url,
                'name': card.name,
                'edition': card.edition.name,
                'type': card.type,
                'sub_types': card.sub_types,
                'cost': card.cost,
                'rarity': card.rarity,
            })
    return results

def post_add_to_favorites(request):
    results = {'success': False, 'message': 'You are not logged in.'}
    card_id = request.POST.get('card_id', None)
    user = get_user(request)
    if user:
        card = Card.objects.get(id=card_id)
        try:
            favorite = Favorite.objects.filter(card=card, user=user)[0]
            results['message'] = '%s was already in your favorites.' % card.name
        except IndexError:
            favorite = Favorite(card=card, user=user)
            favorite.save()
            results['message'] = '%s saved to your favorites!' % card.name
            results['success'] = True

    return results
