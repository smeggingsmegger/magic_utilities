(->
  $.ajaxSetup beforeSend: (xhr, settings) ->
    getCookie = undefined
    getCookie = (name) ->
      cookie = undefined
      cookieValue = undefined
      cookies = undefined
      i = undefined
      cookieValue = null
      if document.cookie and document.cookie isnt ""
        cookies = document.cookie.split(";")
        i = 0
        while i < cookies.length
          cookie = jQuery.trim(cookies[i])
          if cookie.substring(0, name.length + 1) is (name + "=")
            cookieValue = decodeURIComponent(cookie.substring(name.length + 1))
            break
          i++
      cookieValue

    xhr.setRequestHeader "X-CSRFToken", getCookie("csrftoken")  unless /^http:.*/.test(settings.url) or /^https:.*/.test(settings.url)

  $(document).on "click", "#card-search-button", ->
    hourglass = $("<img/>",
      src: "/static/images/hourglass.gif"
    )
    $("#results-div").html(hourglass)
    $.post("/ajax/?sys=deck&run=post_search_cards",
      search_string: $('#card-search-input').val()
    , (data) ->
      if data['cards'].length
        if window.location.hostname == "127.0.0.1"
          url_prefix = "http://magiccards.info"
        else
          url_prefix = "/static/"

        table = $("<table/>", "class": "results")
        thead = $("<thead/>")
        thead_row = $("<tr/>")
        th1 = $("<th/>", "html": "")
        th2 = $("<th/>", "html": "Name")
        th3 = $("<th/>", "html": "Edition")
        th4 = $("<th/>", "html": "Type")
        th5 = $("<th/>", "html": "Sub-Type")
        th6 = $("<th/>", "html": "Cast Cost")
        th7 = $("<th/>", "html": "Rarity")
        tbody = $("<tbody/>")
        $(thead_row).append(th1)
        $(thead_row).append(th2)
        $(thead_row).append(th3)
        $(thead_row).append(th4)
        $(thead_row).append(th5)
        $(thead_row).append(th6)
        $(thead_row).append(th7)
        $(thead).append(thead_row)
        $(table).html(thead)
        for index, card of data['cards']
          tr = $("<tr/>")
          a1 = $("<a/>", "href": "#", "class": "add-card", "qtipcontent": card['id'] + "&nbsp;")
          td1 = $("<td/>", "html": a1)
          td2 = $("<td/>", "html": $("<a/>", "href": "/?id=" + card["id"], "class": "hover-image", "qtipcontent": url_prefix + card["image_url"], "html": card["name"]))
          td3 = $("<td/>", "html": card["edition"])
          td4 = $("<td/>", "html": card["type"])
          td5 = $("<td/>", "html": card["sub_types"])
          td6 = $("<td/>", "html": $("<span/>", "class": "card-cost", "html": card["cost"]))
          td7 = $("<td/>", "html": card["rarity"])
          $(tr).append(td1)
          $(tr).append(td2)
          $(tr).append(td3)
          $(tr).append(td4)
          $(tr).append(td5)
          $(tr).append(td6)
          $(tr).append(td7)
          $(tbody).append(tr)
        $(table).append(tbody)
        $("#results-div").html(table)
        attach_qtips()
        $(".card-cost").MTGSymbols replace_more: true
    ).error ->
      alert "Error: could not complete search!"

  $(document).on "click", ".add-to-favorites", ->
    $.post("/ajax/?sys=deck&run=post_add_to_favorites",
      card_id: $(this).attr("card_id")
    , (data) ->
      alert data["message"]
    ).error ->
      alert "Error: could not add to favorites!"

  $(document).on "click", "#create-deck-submit", ->
    $.post("/ajax/?sys=deck&run=post_create_deck",
      deck_name: $('#deck_name').val()
      deck_description: $('#deck_description').val()
    , (data) ->
      if data["success"]
        t1 = $("<td/>", "html": data["deck"]["name"], "class": "td-deck-name")
        t2 = $("<td/>", "html": $("<div/>", "class": "div-deck-description", "html": data["deck"]["description"]), "class": "td-deck-name")
        t3 = $("<td/>", "html": "0")
        t4 = $("<td/>", "width": "1", "html": $("<a/>", "href": "#", "class": "button x-small black select-deck", "deck_id": data["deck"]["id"], "html": $("<span/>", "html": "Select")))
        tr = $("<tr/>")
        tr.append(t1).append(t2).append(t3).append(t4)
        $("#deck-manager-decks-tbody").append(tr)
      alert data["message"]
    ).error ->
      alert "Error: could not add deck!"

  $(document).on "click", "#active-deck", ->
    $("#deck-name").fadeToggle "fast"
    $("#deck-cards").fadeToggle "fast"

  $(document).on "click", ".select-deck", ->
    $.post("/ajax/?sys=deck&run=select_deck",
      deck_id: $(this).attr("deck_id")
    , (data) ->
      alert data["message"]
    ).error ->
      alert "Error: could not select deck!"

  update_deck = ->
    $.post("/ajax/?sys=deck&run=current_deck", (data) ->
      if data['success'] == true
        $("#deck-name").html(data["deck_name"])
        empty_div = $("<div/>", "html": "&nbsp;")
        $("#deck-cards").empty()
        $("#deck-cards").append(empty_div)
        for index, card of data['cards']
          $("#deck-cards").append($("<div/>", "class": "deck-card", "html": card["quantity"] + "x&nbsp;&nbsp;&nbsp;" + card['name']))
        $("#deck-cards").append(empty_div)
      else
        alert(data['message'])
    ).error ->
      alert "Error: could not update deck!"

  attach_qtips = ->
    $(".hover-image").qtip
      content:
        text: (api) ->
          "<img class=\"preview-image\" src=\"" + $(this).attr("qtipcontent") + "\" />"

      position:
        viewport: $(window)
        my: "bottom left"
        at: "middle right"
        target: $(".hover-image")

      style:
        classes: "ui-tooltip-dark ui-tooltip-shadow"
        width: 195
        height: 263

    $(".add-card").qtip
      show:
        event: "click"
        solo: true
        effect: (offset) ->
          $(this).slideDown 100

      hide:
        event: "unfocus"

      content:
        title: "Add to deck:"
        text: (api) ->
          "<form id=\"add-card-popup\">" + "<input type=\"hidden\" value=\"" + $(this).attr("qtipcontent") + "\"/>" + "<input type=\"radio\" name=\"count\" value=\"1\" checked=\"checked\" /><span>1</span>" + "<input type=\"radio\" name=\"count\" value=\"2\" /><span>2</span>" + "<input type=\"radio\" name=\"count\" value=\"3\" /><span>3</span>" + "<input type=\"radio\" name=\"count\" value=\"4\" /><span>4</span>" + "&nbsp;&nbsp;<input type=\"submit\" value=\"Add\" />" + "</form>"

      events:
        render: (event, api) ->
          $(this).first().submit ->
            card_id = undefined
            form = undefined
            num_cards = undefined
            form = $(api.elements.content)
            card_id = form.find("input:hidden").val()
            num_cards = form.find("input:radio[name=count]:checked").val()
            console.log "adding " + num_cards + " of " + card_id
            $.post("/ajax/?sys=deck&run=add_card_to_deck",
              count: num_cards
              card_id: card_id
            , (data) ->
              alert(data['message'])
              if data['success'] == true
                update_deck()
            ).error ->
              alert "Error: could not add card to deck!"

            false

      position:
        viewport: $(window)
        my: "bottom left"
        at: "middle right"
        target: $(".add-card")

      style:
        classes: "ui-tooltip-dark ui-tooltip-shadow"
        width: 188
        height: 57

  $(document).ready ->
    to_replace_elements = undefined
    attach_qtips()
    $(".card-info").MTGSymbols()
    $(".card-cost").MTGSymbols replace_more: true
).call this
