(function() {

  (function() {
    var attach_qtips, update_deck;
    $.ajaxSetup({
      beforeSend: function(xhr, settings) {
        var getCookie;
        getCookie = void 0;
        getCookie = function(name) {
          var cookie, cookieValue, cookies, i;
          cookie = void 0;
          cookieValue = void 0;
          cookies = void 0;
          i = void 0;
          cookieValue = null;
          if (document.cookie && document.cookie !== "") {
            cookies = document.cookie.split(";");
            i = 0;
            while (i < cookies.length) {
              cookie = jQuery.trim(cookies[i]);
              if (cookie.substring(0, name.length + 1) === (name + "=")) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
              }
              i++;
            }
          }
          return cookieValue;
        };
        if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
          return xhr.setRequestHeader("X-CSRFToken", getCookie("csrftoken"));
        }
      }
    });
    $(document).on("click", "#card-search-button", function() {
      var hourglass;
      hourglass = $("<img/>", {
        src: "/static/images/hourglass.gif"
      });
      $("#results-div").html(hourglass);
      return $.post("/ajax/?sys=deck&run=post_search_cards", {
        search_string: $('#card-search-input').val()
      }, function(data) {
        var a1, card, index, table, tbody, td1, td2, td3, td4, td5, td6, td7, th1, th2, th3, th4, th5, th6, th7, thead, thead_row, tr, url_prefix, _ref;
        if (data['cards'].length) {
          if (window.location.hostname === "127.0.0.1") {
            url_prefix = "http://magiccards.info";
          } else {
            url_prefix = "/static/";
          }
          table = $("<table/>", {
            "class": "results"
          });
          thead = $("<thead/>");
          thead_row = $("<tr/>");
          th1 = $("<th/>", {
            "html": ""
          });
          th2 = $("<th/>", {
            "html": "Name"
          });
          th3 = $("<th/>", {
            "html": "Edition"
          });
          th4 = $("<th/>", {
            "html": "Type"
          });
          th5 = $("<th/>", {
            "html": "Sub-Type"
          });
          th6 = $("<th/>", {
            "html": "Cast Cost"
          });
          th7 = $("<th/>", {
            "html": "Rarity"
          });
          tbody = $("<tbody/>");
          $(thead_row).append(th1);
          $(thead_row).append(th2);
          $(thead_row).append(th3);
          $(thead_row).append(th4);
          $(thead_row).append(th5);
          $(thead_row).append(th6);
          $(thead_row).append(th7);
          $(thead).append(thead_row);
          $(table).html(thead);
          _ref = data['cards'];
          for (index in _ref) {
            card = _ref[index];
            tr = $("<tr/>");
            a1 = $("<a/>", {
              "href": "#",
              "class": "add-card",
              "qtipcontent": card['id'] + "&nbsp;"
            });
            td1 = $("<td/>", {
              "html": a1
            });
            td2 = $("<td/>", {
              "html": $("<a/>", {
                "href": "/?id=" + card["id"],
                "class": "hover-image",
                "qtipcontent": url_prefix + card["image_url"],
                "html": card["name"]
              })
            });
            td3 = $("<td/>", {
              "html": card["edition"]
            });
            td4 = $("<td/>", {
              "html": card["type"]
            });
            td5 = $("<td/>", {
              "html": card["sub_types"]
            });
            td6 = $("<td/>", {
              "html": $("<span/>", {
                "class": "card-cost",
                "html": card["cost"]
              })
            });
            td7 = $("<td/>", {
              "html": card["rarity"]
            });
            $(tr).append(td1);
            $(tr).append(td2);
            $(tr).append(td3);
            $(tr).append(td4);
            $(tr).append(td5);
            $(tr).append(td6);
            $(tr).append(td7);
            $(tbody).append(tr);
          }
          $(table).append(tbody);
          $("#results-div").html(table);
          attach_qtips();
          return $(".card-cost").MTGSymbols({
            replace_more: true
          });
        }
      }).error(function() {
        return alert("Error: could not complete search!");
      });
    });
    $(document).on("click", ".add-to-favorites", function() {
      return $.post("/ajax/?sys=deck&run=post_add_to_favorites", {
        card_id: $(this).attr("card_id")
      }, function(data) {
        return alert(data["message"]);
      }).error(function() {
        return alert("Error: could not add to favorites!");
      });
    });
    $(document).on("click", "#create-deck-submit", function() {
      return $.post("/ajax/?sys=deck&run=post_create_deck", {
        deck_name: $('#deck_name').val(),
        deck_description: $('#deck_description').val()
      }, function(data) {
        var t1, t2, t3, t4, tr;
        if (data["success"]) {
          t1 = $("<td/>", {
            "html": data["deck"]["name"],
            "class": "td-deck-name"
          });
          t2 = $("<td/>", {
            "html": $("<div/>", {
              "class": "div-deck-description",
              "html": data["deck"]["description"]
            }),
            "class": "td-deck-name"
          });
          t3 = $("<td/>", {
            "html": "0"
          });
          t4 = $("<td/>", {
            "width": "1",
            "html": $("<a/>", {
              "href": "#",
              "class": "button x-small black select-deck",
              "deck_id": data["deck"]["id"],
              "html": $("<span/>", {
                "html": "Select"
              })
            })
          });
          tr = $("<tr/>");
          tr.append(t1).append(t2).append(t3).append(t4);
          $("#deck-manager-decks-tbody").append(tr);
        }
        return alert(data["message"]);
      }).error(function() {
        return alert("Error: could not add deck!");
      });
    });
    $(document).on("click", "#active-deck", function() {
      $("#deck-name").fadeToggle("fast");
      return $("#deck-cards").fadeToggle("fast");
    });
    $(document).on("click", ".select-deck", function() {
      return $.post("/ajax/?sys=deck&run=select_deck", {
        deck_id: $(this).attr("deck_id")
      }, function(data) {
        return alert(data["message"]);
      }).error(function() {
        return alert("Error: could not select deck!");
      });
    });
    update_deck = function() {
      return $.post("/ajax/?sys=deck&run=current_deck", function(data) {
        var card, empty_div, index, _ref;
        if (data['success'] === true) {
          $("#deck-name").html(data["deck_name"]);
          empty_div = $("<div/>", {
            "html": "&nbsp;"
          });
          $("#deck-cards").empty();
          $("#deck-cards").append(empty_div);
          _ref = data['cards'];
          for (index in _ref) {
            card = _ref[index];
            $("#deck-cards").append($("<div/>", {
              "class": "deck-card",
              "html": card["quantity"] + "x&nbsp;&nbsp;&nbsp;" + card['name']
            }));
          }
          return $("#deck-cards").append(empty_div);
        } else {
          return alert(data['message']);
        }
      }).error(function() {
        return alert("Error: could not update deck!");
      });
    };
    attach_qtips = function() {
      $(".hover-image").qtip({
        content: {
          text: function(api) {
            return "<img class=\"preview-image\" src=\"" + $(this).attr("qtipcontent") + "\" />";
          }
        },
        position: {
          viewport: $(window),
          my: "bottom left",
          at: "middle right",
          target: $(".hover-image")
        },
        style: {
          classes: "ui-tooltip-dark ui-tooltip-shadow",
          width: 195,
          height: 263
        }
      });
      return $(".add-card").qtip({
        show: {
          event: "click",
          solo: true,
          effect: function(offset) {
            return $(this).slideDown(100);
          }
        },
        hide: {
          event: "unfocus"
        },
        content: {
          title: "Add to deck:",
          text: function(api) {
            return "<form id=\"add-card-popup\">" + "<input type=\"hidden\" value=\"" + $(this).attr("qtipcontent") + "\"/>" + "<input type=\"radio\" name=\"count\" value=\"1\" checked=\"checked\" /><span>1</span>" + "<input type=\"radio\" name=\"count\" value=\"2\" /><span>2</span>" + "<input type=\"radio\" name=\"count\" value=\"3\" /><span>3</span>" + "<input type=\"radio\" name=\"count\" value=\"4\" /><span>4</span>" + "&nbsp;&nbsp;<input type=\"submit\" value=\"Add\" />" + "</form>";
          }
        },
        events: {
          render: function(event, api) {
            return $(this).first().submit(function() {
              var card_id, form, num_cards;
              card_id = void 0;
              form = void 0;
              num_cards = void 0;
              form = $(api.elements.content);
              card_id = form.find("input:hidden").val();
              num_cards = form.find("input:radio[name=count]:checked").val();
              console.log("adding " + num_cards + " of " + card_id);
              $.post("/ajax/?sys=deck&run=add_card_to_deck", {
                count: num_cards,
                card_id: card_id
              }, function(data) {
                alert(data['message']);
                if (data['success'] === true) return update_deck();
              }).error(function() {
                return alert("Error: could not add card to deck!");
              });
              return false;
            });
          }
        },
        position: {
          viewport: $(window),
          my: "bottom left",
          at: "middle right",
          target: $(".add-card")
        },
        style: {
          classes: "ui-tooltip-dark ui-tooltip-shadow",
          width: 188,
          height: 57
        }
      });
    };
    return $(document).ready(function() {
      var to_replace_elements;
      to_replace_elements = void 0;
      attach_qtips();
      $(".card-info").MTGSymbols();
      return $(".card-cost").MTGSymbols({
        replace_more: true
      });
    });
  }).call(this);

}).call(this);
