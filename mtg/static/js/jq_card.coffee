$ = jQuery

$.fn.MTGSymbols = (options) ->
  enclosures = [["[", "]"], ["{", "}"]]
  symbols = ["G P", "T", "B P", "R P", "U P", "W P", "R W", "R G", "B G", "B R", "G R", "G U", "G W", "U B", "U R", "W B", "W U", "2 B", "2 G", "2 R", "2 W", "2 U", "2 P"]
  more_symbols = ["G", "R", "B", "U", "W", "X", "Y", "Z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
  replacement_url_parts = ["<img src=\"/static/images/symbols/", ".png\" class=\"symbol\" />"]
  special_replacements =
    1: "one"
    2: "two"
    3: "three"
    4: "four"
    5: "five"
    6: "six"
    7: "seven"
    8: "eight"
    9: "nine"
    0: "zero"
  defaults =
    replace_more: false

  options = $.extend(defaults, options)
  full_symbols = []
  for enc_index, enclosure of enclosures
    for index, symbol of symbols
      full_symbols.push(enclosure[0] + symbol.replace(" ", "\/") + enclosure[1])
      full_symbols.push(enclosure[0] + symbol.replace(" ", "") + enclosure[1])
    for index, symbol of more_symbols
      full_symbols.push(enclosure[0] + symbol.replace(" ", "\/") + enclosure[1])
      full_symbols.push(enclosure[0] + symbol.replace(" ", "") + enclosure[1])

  @each ->
    html_value = $(this).html()

    for index, symbol of full_symbols
      if html_value.indexOf(symbol) != -1
        symbol_image_name = symbol.replace(/[^a-zA-Z0-9]+/g, '').toLowerCase()
        for idx, row of special_replacements
          symbol_image_name = symbol_image_name.replace(idx, row)

        symbol_replacement = replacement_url_parts[0] + symbol_image_name + replacement_url_parts[1]
        re = new RegExp(symbol.replace(/([-.*+?^${}()|[\]\/\\])/g, '\\$1'), "g")
        html_value = html_value.replace(re, symbol_replacement)

    if options.replace_more
      for index, symbol of more_symbols
        re = new RegExp(symbol, "g")
        if symbol in special_replacements
          html_value = html_value.replace(re, replacement_url_parts[0] + special_replacements[symbol] + replacement_url_parts[1])
        else
          html_value = html_value.replace(re, replacement_url_parts[0] + symbol.toLowerCase() + replacement_url_parts[1])

    $(this).html(html_value)
