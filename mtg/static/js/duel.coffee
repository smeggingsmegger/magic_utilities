socket = io.connect Global.duelserver 

socket.on 'error', (reason) ->
    console.log 'Lost connection:', reason 

socket.emit 'client.nick', { username: Global.username } 

socket.on 'broadcast.userlist', (data) ->
    console.log "updating list of #{ data.users.length } users"

    userlist = $('#users')
    userlist.empty()
    userlist.append "<li>#{ elem } </li>" for elem in data.users

socket.on 'broadcast.message', (data) ->
    console.log "got message: #{ data.from }:#{ data.text }"

    chat = $('#chat')
    if data.me 
        chat.append "<div class='me'><span></span>#{ data.from  } #{ data.text }</div>"
    else
        chat.append "<div><span>#{ data.from }</span>#{ data.text }</div>"

    # TODO: 100 is a fudge because i cant get the horiz scrollbar off my screen !!!
    chat.animate { scrollTop: chat[0].scrollHeight - chat.height() + 100 }, 250

send_message = () ->
    msg = $('#message')
    return if !msg # dont send blanks out

    args = /\s*\/(\w+)\s((\w+).*)/g.exec msg.val()

    if !args
        console.log "sending '#{ msg.val() }'"
        socket.emit 'client.message', { text: msg.val() }
    else
        switch args[1]
            when 'nick'
                socket.emit 'client.nick', { username: args[3] }
            when 'me'
                socket.emit 'client.me', { text: args[2] }
            else
                console.log "sending '#{ msg.val() }'"
                socket.emit 'client.message', { text: msg.val() }

    msg.val ''

$ () ->

    $('#lobby').submit (event) ->
        send_message()
        return false

    $('#message').keypress (event) ->
        code = event.keyCode or event.which
        if code == 13
            send_message()
            return false

