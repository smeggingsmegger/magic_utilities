$ = jQuery

$.fn.saveDecks = (options) ->
  defaults =
    public: false

  options = $.extend(defaults, options)

  @each ->
#      post_object = $(this).serialize()
    $.post("/save_decks/", $(this).serialize())
