(function() {
  var send_message, socket;

  socket = io.connect(Global.duelserver);

  socket.on('error', function(reason) {
    return console.log('Lost connection:', reason);
  });

  socket.emit('client.nick', {
    username: Global.username
  });

  socket.on('broadcast.userlist', function(data) {
    var elem, userlist, _i, _len, _ref, _results;
    console.log("updating list of " + data.users.length + " users");
    userlist = $('#users');
    userlist.empty();
    _ref = data.users;
    _results = [];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      elem = _ref[_i];
      _results.push(userlist.append("<li>" + elem + " </li>"));
    }
    return _results;
  });

  socket.on('broadcast.message', function(data) {
    var chat;
    console.log("got message: " + data.from + ":" + data.text);
    chat = $('#chat');
    if (data.me) {
      chat.append("<div class='me'><span></span>" + data.from + " " + data.text + "</div>");
    } else {
      chat.append("<div><span>" + data.from + "</span>" + data.text + "</div>");
    }
    return chat.animate({
      scrollTop: chat[0].scrollHeight - chat.height() + 100
    }, 250);
  });

  send_message = function() {
    var args, msg;
    msg = $('#message');
    if (!msg) return;
    args = /\s*\/(\w+)\s((\w+).*)/g.exec(msg.val());
    if (!args) {
      console.log("sending '" + (msg.val()) + "'");
      socket.emit('client.message', {
        text: msg.val()
      });
    } else {
      switch (args[1]) {
        case 'nick':
          socket.emit('client.nick', {
            username: args[3]
          });
          break;
        case 'me':
          socket.emit('client.me', {
            text: args[2]
          });
          break;
        default:
          console.log("sending '" + (msg.val()) + "'");
          socket.emit('client.message', {
            text: msg.val()
          });
      }
    }
    return msg.val('');
  };

  $(function() {
    $('#lobby').submit(function(event) {
      send_message();
      return false;
    });
    return $('#message').keypress(function(event) {
      var code;
      code = event.keyCode || event.which;
      if (code === 13) {
        send_message();
        return false;
      }
    });
  });

}).call(this);
