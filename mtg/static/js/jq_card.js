(function() {
  var $,
    __indexOf = Array.prototype.indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  $ = jQuery;

  $.fn.MTGSymbols = function(options) {
    var defaults, enc_index, enclosure, enclosures, full_symbols, index, more_symbols, replacement_url_parts, special_replacements, symbol, symbols;
    enclosures = [["[", "]"], ["{", "}"]];
    symbols = ["G P", "T", "B P", "R P", "U P", "W P", "R W", "R G", "B G", "B R", "G R", "G U", "G W", "U B", "U R", "W B", "W U", "2 B", "2 G", "2 R", "2 W", "2 U", "2 P"];
    more_symbols = ["G", "R", "B", "U", "W", "X", "Y", "Z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
    replacement_url_parts = ["<img src=\"/static/images/symbols/", ".png\" class=\"symbol\" />"];
    special_replacements = {
      1: "one",
      2: "two",
      3: "three",
      4: "four",
      5: "five",
      6: "six",
      7: "seven",
      8: "eight",
      9: "nine",
      0: "zero"
    };
    defaults = {
      replace_more: false
    };
    options = $.extend(defaults, options);
    full_symbols = [];
    for (enc_index in enclosures) {
      enclosure = enclosures[enc_index];
      for (index in symbols) {
        symbol = symbols[index];
        full_symbols.push(enclosure[0] + symbol.replace(" ", "\/") + enclosure[1]);
        full_symbols.push(enclosure[0] + symbol.replace(" ", "") + enclosure[1]);
      }
      for (index in more_symbols) {
        symbol = more_symbols[index];
        full_symbols.push(enclosure[0] + symbol.replace(" ", "\/") + enclosure[1]);
        full_symbols.push(enclosure[0] + symbol.replace(" ", "") + enclosure[1]);
      }
    }
    return this.each(function() {
      var html_value, idx, index, re, row, symbol, symbol_image_name, symbol_replacement;
      html_value = $(this).html();
      for (index in full_symbols) {
        symbol = full_symbols[index];
        if (html_value.indexOf(symbol) !== -1) {
          symbol_image_name = symbol.replace(/[^a-zA-Z0-9]+/g, '').toLowerCase();
          for (idx in special_replacements) {
            row = special_replacements[idx];
            symbol_image_name = symbol_image_name.replace(idx, row);
          }
          symbol_replacement = replacement_url_parts[0] + symbol_image_name + replacement_url_parts[1];
          re = new RegExp(symbol.replace(/([-.*+?^${}()|[\]\/\\])/g, '\\$1'), "g");
          html_value = html_value.replace(re, symbol_replacement);
        }
      }
      if (options.replace_more) {
        for (index in more_symbols) {
          symbol = more_symbols[index];
          re = new RegExp(symbol, "g");
          if (__indexOf.call(special_replacements, symbol) >= 0) {
            html_value = html_value.replace(re, replacement_url_parts[0] + special_replacements[symbol] + replacement_url_parts[1]);
          } else {
            html_value = html_value.replace(re, replacement_url_parts[0] + symbol.toLowerCase() + replacement_url_parts[1]);
          }
        }
      }
      return $(this).html(html_value);
    });
  };

}).call(this);
