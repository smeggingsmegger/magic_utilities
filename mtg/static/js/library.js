ReqJSON = {

    Implements: [Options, Events],

    /**
     * Method: initialize
     *
     * Parameters:
     *  options (object) - the options object
     */
    initialize: function(options) {

        var self = this;

        self.setOptions(options);
        self.response = null;
        return self;
    },

    /**
     * Method: send
     *
     * Parameters:
     *  none
     */
    send: function(theJSON) {
        var self = this;
        self.response = new Request.JSON({
            'url': self.options.url,
            'onSuccess': function (responseJSON) {
                if (typeOf(responseJSON) == 'array') {
                    responseJSON = [responseJSON];
                }
                self.fireEvent('onSuccess', responseJSON);
            }
        }).get(theJSON);
        return self;
    }

};
ReqJSON = new Class(ReqJSON);

