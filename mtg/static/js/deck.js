(function() {
  var $;

  $ = jQuery;

  $.fn.saveDecks = function(options) {
    var defaults;
    defaults = {
      public: false
    };
    options = $.extend(defaults, options);
    return this.each(function() {
      return $.post("/save_decks/", $(this).serialize());
    });
  };

}).call(this);
