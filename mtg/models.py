from django.db import models
from django.contrib.auth.models import User

_TYPES = [
    (u'T', u''),
    (u'P', u''),
    (u'E', u''),
    (u'L', u''),
]

class Edition(models.Model):
    name = models.CharField(max_length=64)
    url = models.CharField(max_length=128)
    category = models.CharField(max_length=128)
    completed = models.BooleanField(default=False)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    class Meta:
        db_table = u'editions'

class Card(models.Model):
    name = models.CharField(max_length=256)
    mci_id = models.CharField(max_length=36, null=True)
    type = models.CharField(max_length=128, null=True)
    sub_types = models.CharField(max_length=128, null=True)
    cost = models.CharField(max_length=128, null=True)
    converted_cost = models.IntegerField(null=True)
    abilities = models.CharField(max_length=1024, null=True)
    oracle_text = models.CharField(max_length=1024, null=True)
    flavor_text = models.CharField(max_length=1024, null=True)
    legalities = models.TextField(null=True)
    rulings = models.TextField(null=True)
    power = models.CharField(max_length=16, null=True)
    toughness = models.CharField(max_length=16, null=True)
    illustrator = models.CharField(max_length=256, null=True)
    rarity = models.CharField(max_length=128, null=True)
    edition = models.ForeignKey(Edition)
    other_side = models.CharField(max_length=36, null=True)
    url = models.CharField(max_length=256, null=True)
    completed = models.BooleanField(default=False)
    image_url = models.CharField(max_length=256, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    class Meta:
        db_table = u'cards'

class Deck(models.Model):
    user = models.ForeignKey('auth.User', null=True)
    name = models.CharField(max_length=256, null=True)
    description = models.TextField(null=True)
    cards = models.ManyToManyField('Card', through="DeckCard")

    def quantity(self):
        return sum(self.deckcard_set.all().values_list('quantity', flat=True))

    def get_cards(self):
        """
        Returns a list of all cards in the deck with duplicates as necessary.
        """
        out = []
        for dc in DeckCard.objects.filter(deck=self).select_related('card'):
            if dc.quantity == 1:
                out.append(dc.card)
            else:
                out += [dc.card] * dc.quantity
        return out

    def deck_cards(self):
        return DeckCard.objects.filter(deck=self)

    class Meta:
        db_table = u'decks'

class DeckCard(models.Model):
    deck = models.ForeignKey('Deck')
    card = models.ForeignKey('Card')
    quantity = models.IntegerField(default=1)

    class Meta:
        db_table = u'deck_cards'

class Favorite(models.Model):
    card = models.ForeignKey('Card', null=True)
    user = models.ForeignKey('auth.User', null=True)

    class Meta:
        db_table = u'favorites'


# class Player(models.Model):
#     name = models.CharField(max_length=256)
#     birthdate = models.DateField(null=True)
#     ranking = models.CharField(max_length=32, null=True)
#     email = models.CharField(max_length=64, null=True)
#     created_on = models.DateTimeField(auto_now_add=True)
#     updated_on = models.DateTimeField(auto_now=True)
#     class Meta:
#         db_table = u'players'

class Tourney(models.Model):
    TYPES = (
        ('SI', 'Single Elimination'),
        ('SW', 'Swiss'),
        ('RO', 'Round-Robin'),
        ('HI', 'Hitlist'),
    )
    name = models.CharField(max_length=256)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField(null=True)
    forbid_organizer = models.BooleanField(default=False)
    playoff = models.BooleanField(default=False)
    playoff_type = models.CharField(max_length=2, choices=TYPES, null=True)
    playoff_time = models.IntegerField(null=True)
    round_time = models.IntegerField(null=True)
    tourney_type = models.CharField(max_length=2, choices=TYPES)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    class Meta:
        db_table = u'tournaments'

class TourneyMatch(models.Model):
    tournament = models.ForeignKey(Tourney, related_name='+')
    first_player = models.ForeignKey(User, related_name='+')
    second_player = models.ForeignKey(User, related_name='+')
    round_number = models.IntegerField()
    is_playoff = models.BooleanField(default=False)
    winner = models.ForeignKey(Tourney, null=True, related_name='+')
    is_forfeit = models.BooleanField(default=False)
    start_date = models.DateTimeField(null=True)
    end_date = models.DateTimeField(null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    class Meta:
        db_table = u'matches'

class TourneyOrganizers(models.Model):
    organizer = models.ForeignKey(User)
    tournament = models.ForeignKey(Tourney)
    class Meta:
        db_table = u'tournament_organizers'

class TourneyPlayers(models.Model):
    player = models.ForeignKey(User)
    tournament = models.ForeignKey(Tourney)
    class Meta:
        db_table = u'tournament_players'
