# from django import forms

# from magic_utilities.mtg.models import Deck
# 
# 
# class DeckForm(forms.ModelForm):
#     def __init__(self, user, *args, **kwargs):
#         self.user = user
#         super(DeckForm, self).__init__(*args, **kwargs)
# 
#     def save(self, commit=True):
#         out = super(DeckForm, self).save(commit=False)
#         out.user = self.user
#         if commit:
#             out.save()
#         return out
# 
#     class Meta:
#         model = Deck
#         fields = ('title',)
