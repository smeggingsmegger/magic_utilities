import random


class Simulator:

    @staticmethod
    def humanoid_shuffle(items, num_shuffles=6):
        MAX_STREAK = 10

        # divide list roughly in half
        num_items = len(items)
        end_range = int(num_items / 2 + random.randint(0, int(.1 * num_items)))
        first_half = items[:end_range]  # deck up to 0 - end_range
        second_half = items[end_range:]  # deck after end_range - len(items)

        split_decks = (first_half, second_half)
        mixed = []
        streak = current_deck_index = 0
        # while both lists still contain items
        while first_half and second_half:
            # calc the percentage of remaining total cards
            remaining = (1 - float(len(mixed)) / num_items)
            # if we generate a random value less than the remaining percentage
            # which will be continually be decreasing (along with the probability)
            # or
            # if MAX_STREAK is exceeded
            if random.random() < remaining or streak > MAX_STREAK:
                 # switch which deck is being used to pull items from
                current_deck_index = 1 ^ current_deck_index
                # reset streak counter
                streak = 0

            # pop the selected list onto the new (shuffled) list
            mixed.append(split_decks[current_deck_index].pop())
            # increment streak of how many consecutive times a list has remained selected
            streak += 1

        # add any remaining cards
        mixed.extend(first_half)
        mixed.extend(second_half)

        num_shuffles -= 1
        # if we still have shuffles to do
        if num_shuffles:
            # rinse and repeat
            mixed = Simulator.humanoid_shuffle(mixed, num_shuffles)

        # finally return fully shuffled list
        return mixed
