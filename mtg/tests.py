"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from magic_utilities.mtg.models import Tourney, TourneyMatch, TourneyPlayers

class TournamentTest(TestCase):
    def setUp(self):
        from django.contrib.auth.models import User
        from datetime import date
        #set up users
        users = ['john','paul','george','ringo','cassidy','linda','matt','amanda','myles','shadow']
        for user in users:
            luser = User.objects.create_user(user, user+'@theplanesofmagic.com', user+'password')
            luser.save()

        #set up tournament(s)
        self.tournament = Tourney(name="Test Tournament",
                                  start_date=date(2012,4,20),
                                  tourney_type='RO')
        #put players in the tournament
        users = User.objects.all()
        self.matches = []
        for user in users:
            self.matches.append(TourneyPlayers(player=user, tournament=self.tournament))

    def test_matches(self):
        self.assertGreater(len(self.matches), 0, "Matches was zero length")

class SimpleTest(TestCase):
    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.assertEqual(1 + 1, 2)

