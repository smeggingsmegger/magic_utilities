from django.views.decorators.http import require_http_methods
from django.shortcuts import render
from magic_utilities.mtg.models import Card, Edition
import os
import json

SCANS_EXIST = os.path.isdir(os.path.abspath(os.path.join(os.path.dirname(__file__), '../scans')))
COLORS = ['Black', 'Blue', 'Red', 'White', 'Green']

@require_http_methods(["GET", "POST"])
def index(request):
    card_id = request.GET.get('id', '')
    edition_id = request.GET.get('edition_id', '')
    show = request.GET.get('show', '')
    card_type = request.GET.get('type', '')
    sub_type = request.GET.get('sub_type', '')
    rarity = request.GET.get('rarity', '')
    cost = request.GET.get('cost', '')

    if card_id:
        card = card_by_id(card_id)
        card.oracle_text = card.oracle_text.replace('\n', '\n\n')
        page = render(request, 'card.html', {"card": card, 'scans': SCANS_EXIST, 'colors': COLORS})
    elif edition_id:
        edition = Edition.objects.filter(id=edition_id)[0]
        cards = Card.objects.filter(edition=edition).all()
        page = render(request, 'edition.html', {"cards": cards, "edition": edition, 'scans': SCANS_EXIST, 'colors': COLORS})
    elif show == 'editions':
        editions = Edition.objects.all()
        page = render(request, 'editions_list.html', {'editions': editions, 'scans': SCANS_EXIST, 'colors': COLORS})
    elif card_type:
        cards = Card.objects.filter(type=card_type).all()
        page = render(request, 'cards.html', {"cards": cards, 'scans': SCANS_EXIST, 'colors': COLORS})
    elif sub_type:
        cards = Card.objects.filter(sub_types__contains=sub_type).all()
        page = render(request, 'cards.html', {"cards": cards, 'scans': SCANS_EXIST, 'colors': COLORS})
    elif rarity:
        cards = Card.objects.filter(rarity=rarity).all()
        page = render(request, 'cards.html', {"cards": cards, 'scans': SCANS_EXIST, 'colors': COLORS})
    elif cost:
        cards = Card.objects.filter(cost=cost).all()
        page = render(request, 'cards.html', {"cards": cards, 'scans': SCANS_EXIST, 'colors': COLORS})
    else:
        page = search_page(request)

    return page

def search_page(request):
    search_string = request.GET.get('search_string', '')
    search_type = request.GET.get('search_type', '')
    results = []
    return_value = {
        'results': results,
        'searched': False,
        'search_string': search_string,
        'search_type': search_type,
        'scans': SCANS_EXIST,
        'colors': COLORS,
    }
    cards = []
    if search_type == 'Name':
        return_value['searched'] = True
        cards = Card.objects.filter(name__startswith=search_string).all()
    elif search_type == 'Abilities':
        return_value['searched'] = True
        cards = Card.objects.filter(abilities__contains=search_string).all()
    elif search_type == 'Sub-Types':
        return_value['searched'] = True
        cards = Card.objects.filter(sub_types__contains=search_string).all()
    if len(cards):
        for card in cards:
            results.append(card)
    else:
        return show_index(request)
    return_value['results'] = results
    return render(request, 'search.html', return_value)

def cards_by_name(name):
    cards = Card.objects.filter(name=name).all()
    return cards

def card_by_id(card_id):
    card = Card.objects.filter(id=card_id)[0]
    card = format_card(card)
    return card

def format_card(card):
    if card.legalities:
        print card.legalities
        card.legalities = json.loads(card.legalities)
    else:
        card.legalities = []

    if card.rulings:
        card.rulings = json.loads(card.rulings)
    else:
        card.rulings = []

    if card.other_side:
        card.back = Card.objects.get(id=card.other_side)

    card.other_editions = cards_by_name(card.name)
    if len(card.other_editions) == 1:
        card.other_editions = []

    return card


def random_card():
    card = Card.objects.order_by('?')[0]
    card = format_card(card)
    return card

def show_index(request):
    card = random_card()
    card.oracle_text = card.oracle_text.replace('\n', '\n\n')
    return render(request, 'card.html', {"card": card, 'scans': SCANS_EXIST, 'colors': COLORS})
