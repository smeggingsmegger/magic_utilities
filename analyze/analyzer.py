'''A module to analyze a collection of cards for mana distribution and other things.'''
from __future__ import division

class PlaceholderCard(object):
    def __init__(self, cost=0, converted_cost=0, color='red', power=0, toughness=0, card_type='Permanent'):
        self.converted_cost = converted_cost
        self.cost = cost
        self.color = color
        self.power = power
        self.toughness = toughness
        self.card_type = card_type

def get_analyzer_test_deck_list(deck_num = 1):
    """ Get A test deck list."""
    if deck_num == 1:
        return 1, 2, 3, 5, 8, 13, 21, 34, 55, 89
    elif deck_num == 2:
        return 3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5
    elif deck_num ==3:
        return range(30000)
    else:
        return 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 20



def get_deck_list(card_count=10):
    deck_list = []
    for card_num in range(card_count):
        deck_list.append(PlaceholderCard())
    return deck_list

def get_mana_curve(decklist):
    '''Takes a list of cards, and returns a dictionary of
    casting costs as keys, and amount of casting costs in the deck.

    >>> x = PlaceholderCard()
    >>> get_mana_curve([x])
    {0: 1}
    >>> get_mana_curve([x, x, x, x])
    {0: 4}

    >>> y = PlaceholderCard(converted_cost=1)
    >>> get_mana_curve([x, x, y, y])
    {0: 2, 1: 2}

    >>> z = get_deck_list(5)
    >>> # z = create_deck_list(5)
    >>> get_mana_curve(z)
    {0: 5}
    '''

    retval = {}
    for card in decklist:
        retval[card.converted_cost] = retval.get(card.converted_cost, 0)+1
    return retval

def get_color_mana_curve(decklist):
    '''Takes list of cards, seperates into color categories
    then returns values for each card.

    >>> x = PlaceholderCard()
    >>> get_color_mana_curve([x])
    {'blue': {}, 'green': {}, 'white': {}, 'black': {}, 'red': {0: 1}}
    >>> get_color_mana_curve([x, x, x, x])
    {'blue': {}, 'green': {}, 'white': {}, 'black': {}, 'red': {0: 4}}

    '''

    retval = {}

    white={}
    blue={}
    black={}
    red={}
    green={}

    for card in decklist:
        if card.color == 'white':
            white[card.converted_cost] = white.get(card.converted_cost,0) + 1
        if card.color == 'blue':
            blue[card.converted_cost] = blue.get(card.converted_cost,0) + 1
        if card.color == 'black':
            black[card.converted_cost] = black.get(card.converted_cost,0) + 1
        if card.color == 'red':
            red[card.converted_cost] = red.get(card.converted_cost,0) + 1
        if card.color == 'green':
            green[card.converted_cost] = green.get(card.converted_cost,0) + 1

    retval['white'] = white
    retval['blue'] = blue
    retval['black'] = black
    retval['red'] = red
    retval['green'] = green
    return retval

def get_type_distribution(decklist, detail_level=0):
    '''Get a dictionary of card types and the amount of those types in the deck.
    See http://mtg.wikia.com/wiki/Card_Types for more reference.

    The detail_level var could be a constant or somesuch, or we can break these
    bits and pieces out into other functions.

    It knows which card types are lands and which are spells.
    Anything that isn't a land is a spell.
    >>> c = PlaceholderCard(card_type='Creature')
    >>> i = PlaceholderCard(card_type='Instant')
    >>> l = PlaceholderCard(card_type='Land')
    >>> get_type_distribution([c, i, l])
    {'Land': 2, 'Spell': 1}

    It also accepts a detail level, which will let you show more detail (surprise!).
    It knows which card types are permanents, or nonpermanent.
    >>> c = PlaceholderCard(card_type='Creature')
    >>> i = PlaceholderCard(card_type='Instant')
    >>> l = PlaceholderCard(card_type='Land')
    >>> get_type_distribution([c, i, l], detail_level=1)
    {'Permanent': 2, 'NonPermanent': 1}

    It can give even more detailed information, like showing real card types
    like "Artifact" or "Creature".
    >>> c = PlaceholderCard(card_type='Creature')
    >>> i = PlaceholderCard(card_type='Instant')
    >>> l = PlaceholderCard(card_type='Land')
    >>> get_type_distribution([c, i, l], detail_level=2)
    {'Creature': 1, 'Instant': 1, 'Land': 1}

    Or if you give it a higher detail level, it will build a dict of subtypes as well.
    >>> c = PlaceholderCard(card_type='Creature - Goblin')
    >>> i = PlaceholderCard(card_type='Instant - Arcane')
    >>> l = PlaceholderCard(card_type='Land - Forest')
    >>> get_type_distribution([c, i, l], detail_level=3)
    {'Goblin': 1, 'Arcane': 1, 'Forest': 1}
    '''
    pass



def get_card_score(card):
    '''
    Scores a creature card based solely on its cost, power and toughness.
    Lower scores are better.
    May take abilities into account later.

    This is a balanced card. One point of power and toughness per casting cost.
    >>> get_card_score(PlaceholderCard(converted_cost=1, power=1, toughness=1))
    1.0

    This card is a bit expensive. It probably has an ability to make up for it.
    >>> get_card_score(PlaceholderCard(converted_cost=2, power=1, toughness=1))
    2.0

    This card is ridiculously cheap. Why do you not have 4?
    >>> get_card_score(PlaceholderCard(converted_cost=0, power=1, toughness=1))
    0.5

    This is a big, expensive card. However the ratio is right so it's okay.
    Normally you wouldn't want a 1:1 ratio with larger creatures.
    >>> get_card_score(PlaceholderCard(converted_cost=8, power=6, toughness=10))
    1.0
    '''
    # Calculate the mana-per-P/T ratio.

    avg_pt = (card.power+card.toughness)/2

    if card.converted_cost == 0:
        # Special exception for 0 cost cards.
        return (avg_pt/2)
    else:
        return (card.converted_cost/avg_pt)

if __name__ == "__main__":
    import doctest
    errored = False
    errored, test_count = doctest.testmod()

    if not errored:
        print 'Passed %d tests!' % test_count
