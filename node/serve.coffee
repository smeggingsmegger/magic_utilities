fs = require 'fs'
http = require 'http'
sanitize = require 'sanitize'

server = http.createServer (req, res) ->
    res.writeHead 200
    res.end 'Default node handler'

server.listen 8001

io = require('socket.io').listen server

# globals

server_user = "admin"

users = {}
guest_count = 0

# output from server 

broadcast =
    userlist: () ->
        user_names = Object.keys(users)
        io.sockets.emit 'broadcast.userlist', { users: user_names }
        console.log "sending out #{ user_names.length } usernames for update"
    message: (from, text, me=false) ->
        sanitize.strip_tags text, (err, text) ->
            io.sockets.emit 'broadcast.message', { text: text, from: from, me: me }
            console.log "#{ from }: #{ text }"

# input from client 

io.sockets.on 'connection', (socket) ->

    name = ''
    is_guest = false

    socket.on 'client.message', (data) ->
        if data.text
            broadcast.message name, data.text, 

    socket.on 'client.me', (data) ->
        if data.text 
            broadcast.message name, data.text, me=true

    socket.on 'client.nick', (data) ->
        old_name = name
        new_name = data.username

        if !new_name
            new_name = "guest_#{ guest_count+1 }"
            guest_count += 1
            is_guest = true

        new_name = new_name.replace /[ ]/g, "_"

        if users[new_name] or new_name == server_user
            new_name = "#{ new_name }_"
            # TODO: whisper client that his name was taken

        if old_name
            delete users[old_name]
            broadcast.message server_user, "client #{ old_name } now known as #{ new_name}"
        else
            broadcast.message server_user, "random internet lurker is now named #{ new_name }, welcome!"

        name = new_name
        users[new_name] = socket

        broadcast.userlist()

    socket.on 'disconnect', () ->
        delete users[name]
        if is_guest
            guest_count -= 1
        broadcast.message server_user, "client '#{ name }' disconnected, bye!"
        broadcast.userlist()


